/*function getScript(source, callback) {
    var script = document.createElement('script');
    var prior = document.getElementsByTagName('script')[0];
    script.async = 1;

    script.onload = script.onreadystatechange = function(_, isAbort) {
        if(isAbort || !script.readyState || /loaded|complete/.test(script.readyState)) {
            script.onload = script.onreadystatechange = null;
            script = undefined;

            if(!isAbort) { if(callback) callback()}
        }
    };

    script.src = source;
    prior.parentNode.insertBefore(script, prior);
}*/


document.addEventListener("DOMContentLoaded", () => {
    var calendarEl = document.getElementById("calendar");

    var calendar = new FullCalendar.Calendar(calendarEl, {
        plugins: ['dayGrid', 'interaction'],
        header: {
            left: "title",
            right: "prev, next, today",
        },
        defaultView: "dayGridMonth",
        selectable: true,
        selectHelper: true,
        editable: true,
        locale: "fr",
        events: [
            {
                title  : 'event1',
                start  : '2019-10-01'
            },

            {
                title  : 'event2',
                start  : '2019-09-15',
                end    : '2019-09-18'
            },

            {
                title  : 'event3',
                start  : '2019-09-29T12:30:00',
                allDay : false // will make the time show
            },

            {
                title : 'event4',
                start : '2019-10-03T09:00:00',
                end : '2019-10-03T17:00:00',
                allDay : false
            },

            {
                title : 'event5',
                start : '2019-10-09',
                end : '2019-10-15'
            },
        ],

        /*select: function (start, end) {
            getScript('events/new', () => {
                document.getElementById("event_date_range").val(moment(start).format("MM/DD/YYYY HH:mm") + " - " + moment(end).format("MM/DD/YYYY HH:mm"));
                date_range_picker();
                document.getElementsByClassName("start_hidden").val(moment(start).format("YYYY-MM-DD HH:mm"));
                document.getElementsByClassName("end_hidden").val(moment(end).format("YYYY-MM-DD HH:mm"));
            });

            calendar.FullCalendar('unselect');
        },*/

        dateClick: function(info) {
            alert('Date: ' + info.dateStr);
        }
    });

    calendar.render();
});