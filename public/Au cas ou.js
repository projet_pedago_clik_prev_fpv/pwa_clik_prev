// LEAFLET //////////////////////////////////// 

var mymap = L.map('mapid').setView([43.303059, 1.224369], 10);

L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    maxZoom: 18,
    id: 'mapbox.streets',
    accessToken: 'pk.eyJ1IjoicGF0ZXJuIiwiYSI6ImNqemF5ajMxdzAyYTEzY28zbTRyYzR3dDYifQ.Qi8_h0b6l8BDwvt4TyJ5YQ'
}).addTo(mymap);

var popup = L.popup();
function onMapClick(e) {
	popup
		.setLatLng(e.latlng)
		.setContent("You clicked the map at " + e.latlng.toString())
		.openOn(mymap);
}
mymap.on('click', onMapClick);

//var marker = L.marker([43.303059, 1.224369]).addTo(mymap).bindTooltip("Simplon Carbonne");



// GEOLOC ////////////////

if ("geolocation" in navigator) {
  alert("geoloc ok")
} else {
  alert("pas de geoloc")
}
