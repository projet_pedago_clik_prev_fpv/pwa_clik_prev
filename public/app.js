console.log("et bim un console.log de chatons ! (✿◠‿◠)");

////// ------- bouton install PWA     
let deferredPrompt = null;

window.addEventListener('beforeinstallprompt', (e) => {
  // Prevent Chrome 67 and earlier from automatically showing the prompt
  e.preventDefault();
  // Stash the event so it can be triggered later.
  deferredPrompt = e;
});

async function installpwa() {
  console.log('Bouton d\' installation cliqué !');
  if (deferredPrompt) {
    deferredPrompt.prompt();

    deferredPrompt.userChoice.then(function(choiceResult){
      
      if (choiceResult.outcome === 'accepted') {
        console.log('La PWA à bien été installée par le client ! 😍');
      } else {
        console.log('Le client à refusé l\'installation de la PWA ! 😢');
      }
      
      deferredPrompt = null;
    });
  }
}; 

// LEAFLET /////////////////////////////////////////////////////////////

// GEOLOC ///////////////////////////////////
/*
if ("geolocation" in navigator) {
  alert("geoloc ok")
} else {
  alert("pas de geoloc")
}
*/

var mymap = L.map('mapid').fitWorld();
L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    maxZoom: 18,
    id: 'mapbox.streets',
    accessToken: 'pk.eyJ1IjoicGF0ZXJuIiwiYSI6ImNqemF5ajMxdzAyYTEzY28zbTRyYzR3dDYifQ.Qi8_h0b6l8BDwvt4TyJ5YQ'
}).addTo(mymap);


mymap.locate({setView: true, maxZoom: 14});

function onLocationFound(e) {
  var radius = e.accuracy / 2;
	L.marker(e.latlng).addTo(mymap)
		//.bindPopup("You are within " + radius + " meters from this point").openPopup();
	L.circle(e.latlng, radius).addTo(mymap);
}
mymap.on('locationfound', onLocationFound);
function onLocationError(e) {
	alert(e.message);
}
mymap.on('locationerror', onLocationError);

// LEAFLET ROUTING MACHINE ////////////////////////////////

L.Routing.control({
  waypoints: [
      L.latLng(43.1, 0.72),
      L.latLng(43.303059, 1.224369)
  ],
  routeWhileDragging: true
}).addTo(mymap);


// MENU display ///////////////////////////////

function id(el) {
    return document.getElementById(el);
  }

function affichage_menu() {
    var x = document.getElementById("menu");
    if (x.style.display === "block") {
        x.style.display = "none";
      } else {
        x.style.display = "block";
        id("menu").classList.add("ouvrir");
      }
    console.log("au moins le console.log fonctionne...")
  }


