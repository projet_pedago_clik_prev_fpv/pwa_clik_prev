### Listes des idées de modifications de l'agencement de l'appli ###

- Rendre la partie "Prévention" public (identification non requise)

- Utiliser les API des réseaux sociaux pour faciliter la connection

- Api Géoloc (GMaps, Leaflet)

- Utilisation d'un framework CSS
	##### https://colorlib.com/wp/free-css3-frameworks/ (Liste de  21 frameworks)

- Utilisation de SQLite (recherches de tutos et de docs)
	##### Tuto SQLite + Node.js :
	##### https://www.sqlitetutorial.net/sqlite-nodejs/

- Utilisation de PUG comme moteur de template


- Création d'un prototype à peu près viable sur Marvel APP ( https://marvelapp.com/cf604j1 )

- site de création d'avatars gratuit, utilisables sur site internet ( https://www.avatar-gratuit.com/ )
possibilité de mettre en fond transparent.

- lien prév alcool :
( - https://www.youtube.com/watch?v=Ft6xrEsa2pQ
 - http://www.vinetsociete.fr/prevention/entre-jeunes
 - http://www.vinetsociete.fr/prevention/entre-jeunes#gaya_editorial_index--1766
 - simulation peine conduite alcoolisé : <iframe width="750" height="492" scrolling="no" frameborder="0" marginheight="0" marginwidth="0" src="https://modules.securite-routiere.gouv.fr/module_alcool.html"></iframe>
 - Campagne Santé Publique France a fait appel à Marty et Mcfly, deux YouTubeurs ayant une communauté de plus de trois millions d’abonnés. S’ajoute une liste d’artistes et de YouTubeurs célèbres : Les L.E.J, Natoo, Akim Omiri, Seb la Frite et Amixem : https://youtu.be/Uu5sTjwCvCI
 - https://www.youtube.com/watch?v=_6I8zZcVc1w
 - http://jeunes.alcool-info-service.fr/alcool-et-vous/exces-alcool-que-faire
 - petit questionnaire en VRAIS/FAUX ( http://jeunes.alcool-info-service.fr/alcool/Vrai-Faux/ )
 - test (~20min) anonyme pour déterminer ton rapport à l'alcool ( https://www.bourredesavoir.ch/quizz/consommation-alcool/ )
 - 
 )

 - liens prev drogues : 
 ( - https://www.securite-routiere.gouv.fr/dangers-de-la-route/la-drogue-et-la-conduite ( <iframe width="750" height="492" scrolling="no" frameborder="0" marginheight="0" marginwidth="0" src="https://modules.securite-routiere.gouv.fr/module_drogue.html"></iframe> )
 - 
 )

 - liens prev sex : 
 (
 - planning familial ( Numéro vert anonyme & gratuit 0 800 08 11 11 )
 - près de chez toi ( https://www.planning-familial.org/fr/pres-de-chez-vous )
 - https://www.planning-familial.org/fr
- https://educationsexuelle.com/
 )

- text contenus Sexualité (
Le planning familial est une organisation qui peux t'aider à te renseigner sur la sexualité en général. Ils millitent Pour vivre sa sexualité librement ainsi que pour défendre les droits dans le domaine de la santé sexuelle et l’égalité des sexes et des sexualités.


Ils proposent des séances d’éducation à la sexualité, basée sur la libération de la parole et les échanges de points de vue, permet de :
¤¤ déconstruire les idées reçues sur les questions relatives à la sexualité et l’anatomie ;
¤¤ lever les tabous dès le plus jeune âge ;
¤¤ favoriser les échanges entre pairs ;
« lutter contre le sexisme, les LGBTphobies et toutes autres violences liées à la sexualité ». 

En cela, cette approche est globale et positive. Elle favorise la construction d’un socle social pour vivre ensemble, que l’on soit fille ou garçon dans le respect des personnes.

Rapproche-toi du planning familial le plus proche de chez toi {{lien vers 'près de chez toi '}}
) 

 - text contenus prev Général (
- Numéros d'urgences ( numéros gratuits )
 Click N Prev n'est pas un service d'urgences.
Si tu as besoin d'une aide immédiate, tu peux contacter l'un des numéros suivants:

 Numéros Urgence Européen 112 (Si tu es victime ou témoins d'un accident dns un pays de L'Union Européenne)

 SAMU 15 ( Pour obtenir l'intervention d'une équipe médicale lors d'une situation de détresse vitale, ainsi que pour être redirigé vers un organisme de permanence de soins)

 Pompiers 18 (Pour signaler une situation de péril ou un accident concernants des biens ou des personne et obtenir leurs intervention rapide)

 Police/Gendarmerie 17 (Pour signaler une infraction qui nécessite l'intervention imédiate de la police)

 Urgences SOURDS et MALENTENDANTS (Si tu es victime ou témoins d'une situation d'urgenc qui nécessite l'intervention des services de secours. Attenion, service SMS et fax)


 Drogues info service au 0 800 23 13 13, de 8h à 2h, 7 jours sur 7. Votre appel est anonyme et gratuit !
 Tabac info service, au 39 89 (service gratuit + coût de l'appel),
 )

 PLus d'infos sur les principaux numéros d'urgence ( https://www.gouvernement.fr/risques/connaitre-les-numeros-d-urgence )



/////////////////////////////////


 - BBD : 

- id
- pseudo
- mail
- mdp
- téléphone
- avatar ?
- âge;
/////////////////////////////////////////

- texte contenus alcool:
{
	Lorsque vous commencez à vous sentir mal, la première chose à faire est d’arrêter de boire de l’alcool. Mangez et buvez une boisson non alcoolisée, même si vous avez l’impression que vous ne le supporterez pas. Votre corps a besoin d’être réhydraté et de reprendre de l’énergie. Cherchez ce qui pourrait vous soulager, comme par exemple parler, marcher, s’allonger, ou vomir.

Évitez de sortir du lieu où vous vous trouvez sans être accompagné. Vous pourriez vous perdre ou vous endormir dans un endroit non sécurisé. Ne restez pas seul et isolé et n’ayez aucune gêne à faire part de votre malaise à l’un de vos amis. Il pourra vous accompagner dans un endroit calme, prêter attention à votre état et vous rassurer.

Ne prenez pas la route. Même si vous avez l’impression que vous le pouvez, en ayant trop bu vous n’êtes pas le mieux placé pour juger de votre état.

Écouter vos amis quand ils vous disent que vous avez trop bu pourrait vous permettre de vous arrêter de boire avant de vous rendre malade.

Voici quelques liens qui peuvent t'éclairer :  (((   Mettre les liens par ici !! )))
}