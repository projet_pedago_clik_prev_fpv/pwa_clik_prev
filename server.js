const express = require("express");
const port = 6789;
const pug = require("pug");
const sqlite3 = require("sqlite3");
const bodyParser = require("body-parser");

const app = express();
let userid = null;

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.set("view engine", "pug");

app.get("/", (req, res) => {
    res.render("accueil")
});

app.get("/accueil", (req, res) => {
    res.render("accueil")
});

app.get("/prevention", (req, res) => {
    res.render("prevention")
});

app.get("/conseils", (req, res) => {
    res.render("conseils")
});

app.get("/alcool", (req, res) => {
    res.render("alcool")
});

app.get("/drogues", (req, res) => {
    res.render("drogues")
});

app.get("/sexualite", (req, res) => {
    res.render("sexualite")
});

app.get("/connexion", (req, res) => {
    res.render("connexion")
});
app.post("/connexion", (req, res) => {
    console.log("from connexion")
    console.log(req.body)
});

app.get("/inscription", (req, res) => {
    res.render("inscription")
});
app.post("/inscription", (req, res) => {
    console.log("from inscription formulaire")
    console.log(req.body)
});

app.get("/agenda", (req, res) => {
    res.render("agenda")
});

app.get("/carte", (req, res) => {
    res.render("carte")
});
app.post("/carte", (req, res) => {
    console.log("from trajet carte")
    console.log(req.body)
});

app.get("/profil", (req, res) => {
    res.render("profil")
});

app.use(express.static("public"));

console.log("Click n'Prev lancé sur le port " + port);
app.listen(port);

// _-_-_-_-_-_-_-_-_-_-_BDD avec SQlite3

// chemin d'acces à la bdd, sera créé si existe déja /!\
const rootbdd = './data/datapwa.db';
let db = new sqlite3.Database(rootbdd, sqlite3.OPEN_READONLY, err => {
    if (err) {
        return console.log(err.message);
    } else {
        console.log("Connected to the SQLite database.");
    };
}
);

db.each("select rootavatar FROM peoples where id = 1", function (err, patate) {
    if (err) {
        return console.log(err.message);
    } else {
        let testretourdonnee = patate.rootavatar;
        console.log(testretourdonnee);
    };
});

// _-_-_-_-_-_-_-_bonne pratique --> fermer la db après usage !

db.close(function (err) {
    if (!err) {
        console.log("Database's closed successfully !");
    }
    else {
        console.log(err.message);
    }
});

//_-_-_-_-_-_-_-_-_db fermée !


